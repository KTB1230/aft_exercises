// So that i don't have to write gulp. every time
const { src, dest, watch, series, parallel, task } = require('gulp');

// Importing all packages using gulp-load-plugins
// Pattern is important so all plugins are used, not just those with 'gulp-' prefix
const plugins = require('gulp-load-plugins')({
    pattern: '*'
});

// File paths
const paths = {
    styles: {
        src: "src/scss/style.scss",
        dest: "public"
    },
    scripts: {
        src: "src/js/*.js",
        dest: "public"
    },
    images: {
        src: "src/images/*.{gif,jpg,png,svg}",
        dest: "public/img"
    }
}

// Style task: compiles the style.scss file into style.css
function styleTask() {
    return src(paths.styles.src)
        // Initialize sourcemaps before compilation starts
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass())
        .on("error", plugins.sass.logError)
        // Use postcss with autoprefixer and compress the compiled file using cssnano
        .pipe(plugins.postcss([plugins.autoprefixer(), plugins.cssnano()]))
        .pipe(plugins.rename('main.min.css'))
        // Now add/write the sourcemaps
        .pipe(plugins.sourcemaps.write())
        .pipe(dest(paths.styles.dest))
        // Add browsersync stream pipe after compilation
        .pipe(plugins.browserSync.stream());
}

//Lint Task: Check JS for 'errors'
function lintTask() {
    return src(paths.scripts.src)
        .pipe(plugins.eslint())
        .pipe(plugins.eslint.format())
        .pipe(plugins.eslint.failAfterError());
}

//Scripts task: concat all js files and uglify the output
function scriptsTask() {
    return src(paths.scripts.src)
        .pipe(plugins.concat('main.min.js'))
        .pipe(plugins.uglify())
        .pipe(dest(paths.scripts.dest))
        .pipe(plugins.browserSync.stream());
}

//Minify Images task: minify images for better performance
function minImagesTask() {
    return src(paths.images.src)
        .pipe(plugins.imagemin())
        .pipe(dest(paths.images.dest));
}

// A simple task to reload the page
function reload() {
    plugins.browserSync.reload();
}

// Add browsersync initialization at the start of the watch task
function watchTask() {
    plugins.browserSync.init({
        proxy: "http://localhost:63342/aft_exercises/public/",
    });
    watch(paths.styles.src, styleTask);
    watch("public/*.html", reload);
}

// Exposing tasks
exports.watchTask = watchTask;
exports.styleTask = styleTask;
exports.scriptsTask = scriptsTask;
exports.minImagesTaks = minImagesTask;
exports.lintTask = lintTask;

//Specify if tasks run in series or parallel using `series` and `parallel`
var build = parallel(lintTask, styleTask, scriptsTask, minImagesTask, watchTask);


// default task, called by running 'gulp' in CLI
task('default', build);